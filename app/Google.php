<?php

namespace App;

use Google\Cloud\Translate\V2\TranslateClient;
use Illuminate\Database\Eloquent\Model;


class Google extends Model
{
    protected $table = 'google';

    protected $fillable = [
        'updated_at', 'key_user', 'created_at', 'nivel'
    ];

    protected $hidden = [
        'url_credentials',
    ];

    function Translate($data = array())
    {
        try {
            $this->Auth($data['apikey']);
            $translate = new TranslateClient();

            $language = $translate->translate($data['text'], [
                'target' => $data['target']
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => $e->getCode(),
                'error' => $e->getMessage()
            ]);
        }

        return response()->json([
            'from' => $language['source'],
            'to' => $data['target'],
            'input' => $language['input'],
            'result' => $language['text']
        ]);
    }

    function Auth($apikey)
    {
        try {
            $path = Google::where('key_user', '=', $apikey)->first();
            putenv('GOOGLE_APPLICATION_CREDENTIALS='.public_path($path->url_credentials));
        } catch (\Exception $e) {
            return response()->json([
                'status' => $e->getCode(),
                'error' => $e->getMessage()
            ]);
        }

    }
}
