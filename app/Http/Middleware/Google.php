<?php

namespace App\Http\Middleware;

use Closure;
use App\Google as GoogleModel;

class Google
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = GoogleModel::where('key_user', $request->header('apikey'))->first();

        if (isset($result))
            return $next($request);
        else
            return response()->json([
                'status' => 401,
                'error' => 'User unauthorized.'
            ]);
    }
}
