<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Google as Google;


class GoogleController extends Controller
{
    public function Translate(Google $google, Request $request){

        $request->validate([
            'text' => 'required|max:255',
            'target' => 'required',
        ]);
        $data = $request->all();

        $result = $google->Translate($data);

        return $result;
    }
}
