<?php

namespace App\Http\Controllers\Api;

use App\Google;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function permission(Request $request){


        if (Google::where('key_user', $request->system)->first()){
            try {
                $result = Google::create([
                    'key_user' => $request->apikey,
                    'nivel' => $request->nivel,
                ]);
            }catch (\Exception $e) {
                return response()->json([
                   'status' => $e->getCode(),
                   'erro' => $e->getMessage()
                ]);
            }

        }else{
            return response()->json([
                'status' => 400,
                'error' => 'Unauthorized'
            ]);
        }


        if ($result)
            return response()->json([
                'status' => 200,
                'data' => $result
            ]);
        else
            return response()->json([
                'status' => 400,
                'error' => 'Bad request'
            ]);
    }
}
